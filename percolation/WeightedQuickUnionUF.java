public class WeightedQuickUnionUF
{
    private int[] id;
    private int[] sz;

    public WeightedQuickUnionUF(int N){
        id = new int[N];
        sz = new int[N];
        for(int i = 0; i < N; i++){
            id[i] = i;
            sz[i] = 1;
        }
    }

    private int find(int i){
        while(i != id[i]){ i = id[i]; }
        return i;
    }

    private void validate(int p) {
        int n = id.length;
        if (p < 0 || p >= n) {
            throw new IllegalArgumentException("index " + p + " is not between 0 and " + (n-1));
        }
    }

    public boolean connected(int p, int q){
        return (find(p) == find(q));
    }

    public void union(int p, int q){
        int i = find(p);
        int j = find(q);

        if(i == j){ return; }

        if(sz[i] < sz[j]){ id[i] = j; sz[j] += sz[i]; }
        else{ id[j] = i; sz[i] += sz[j]; }
    }
}

    
