// import edu.princeton.cs.algs4.StdOut;
// import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

  private boolean[] isopen;
  private int openSites;
  private WeightedQuickUnionUF wqu;
  private WeightedQuickUnionUF wquBackwash;
  private final int topCore;
  private final int bottomCore;
  private final int n;

  // Create n-by-n grid, with all sites blocked
  public Percolation(int n)
  {
    if (n <= 0)
      throw new java.lang.IllegalArgumentException("n must be higher than 0");
    this.n = n;
    int size = n * n;
    openSites = 0;
    wqu = new WeightedQuickUnionUF(size + 2);
    wquBackwash = new WeightedQuickUnionUF(size + 2);
        
    topCore = 0;
    bottomCore = size + 1;
        
    isopen = new boolean[size + 2];
    for (int row = 1; row <= n; row++) {
      for (int col = 1; col <= n; col++)
        isopen[convertIndexes(row, col)] = false;
    }
    isopen[topCore] = true;
    isopen[bottomCore] = true;
  }

  private void checkIndexes(int row, int col)
  {
    if (row < 1 || row > n)
      throw new java.lang.IllegalArgumentException("row index out of bounds");
    if (col < 1 || col > n)
      throw new java.lang.IllegalArgumentException("col index out of bounds");
  }

  private int convertIndexes(int row, int col)
  {
    return (row - 1) * n + col;
  }

  // Open site (row, col) if it is not open already
  public void open(int row, int col)
  {
    checkIndexes(row, col);
    int index1 = convertIndexes(row, col);

    if (!isopen[index1]) {
      isopen[index1] = true;
      openSites++;

      int index2 = -1;
      if (row == 1)
        index2 = topCore;
      else
        index2 = convertIndexes(row - 1, col);
      if (isopen[index2]) {
        wqu.union(index1, index2);
        wquBackwash.union(index1, index2);
      }
      
      if (col != 1) {
        index2 = convertIndexes(row, col - 1);
        if (isopen[index2]) {
          wqu.union(index1, index2);
          wquBackwash.union(index1, index2);
        }
      }
      
      if (col != n) {
        index2 = convertIndexes(row, col + 1);
        if (isopen[index2]) {
          wqu.union(index1, index2);
          wquBackwash.union(index1, index2);
        }
      }
      
      if (row == n) {
        wqu.union(bottomCore, index1);
      } else {
        index2 = convertIndexes(row + 1, col);
        if (isopen[index2]) {
          wqu.union(index1, index2);
          wquBackwash.union(index1, index2);
        }
      }
    }
  }
  
  // Is site (row, col) open?
  public boolean isOpen(int row, int col)
  {
    checkIndexes(row, col);
    return isopen[convertIndexes(row, col)];
  }

  // is site (row, col) full?
  public boolean isFull(int row, int col)
  {
    checkIndexes(row, col);
    return wquBackwash.connected(topCore, convertIndexes(row, col));
  }

  // Number of open sites
  public int numberOfOpenSites()
  {
    return openSites;
  }

  // Does the system percolate?
  public boolean percolates()
  {
    return wqu.connected(topCore, bottomCore);
  }

  // Test client (optional) 
  public static void main(String[] args)
  {
/*    
    In in = new In(args[0]);
    int n = in.readInt();

    Percolation perc = new Percolation(n);
    
    while (!in.isEmpty()) {
      int row = in.readInt();
      int col = in.readInt();
      perc.open(row, col);
    }
    
    StdOut.println("Percolates? " + perc.percolates());
*/
/*
    int n = 3;
    Percolation perc = new Percolation(n);
    perc.open(3,1);
    StdOut.println("(3,1) open? " + perc.isOpen(3,1));
    StdOut.println("(3,1) full? " + perc.isFull(3,1));
    perc.connected(7,0);

    perc.open(2,3);
    StdOut.println("(3,1) open? " + perc.isOpen(3,1));
    StdOut.println("(3,1) full? " + perc.isFull(3,1));
    perc.connected(7,0);

    perc.open(3,3);
    StdOut.println("(3,1) open? " + perc.isOpen(3,1));
    StdOut.println("(3,1) full? " + perc.isFull(3,1));
    perc.connected(7,0);

    perc.open(1,3);
    StdOut.println("(3,1) open? " + perc.isOpen(3,1));
    StdOut.println("(3,1) full? " + perc.isFull(3,1));
    perc.connected(7,0);

    perc.open(2,1);
    StdOut.println("(3,1) open? " + perc.isOpen(3,1));
    StdOut.println("(3,1) full? " + perc.isFull(3,1));
    perc.connected(7,0);

    perc.open(3,2);
    StdOut.println("(3,1) open? " + perc.isOpen(3,1));
    StdOut.println("(3,1) full? " + perc.isFull(3,1));
    perc.connected(7,0);
*/
  }
}
