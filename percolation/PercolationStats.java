// import edu.princeton.cs.algs4.StdOut;

import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
// import java.lang.Math;

public class PercolationStats
{
  private static final double CONFIDENCE_95 = 1.96;

  private final double mean;
  private final double stddev;
  private final double confidenceLo;
  private final double confidenceHi;

  // Perform trials independent experiments on an n-by-n grid
  public PercolationStats(int n, int trials)
  {
    if (n <= 0)
      throw new java.lang.IllegalArgumentException("n must be higher than 0");
    if (trials <= 0)
      throw new java.lang.IllegalArgumentException("trials must be higher than 0");
    int gridSize = n * n;

    double[] x = new double[trials];

    for (int i = 0; i < trials; i++) {
      Percolation perc = new Percolation(n);
      while (!perc.percolates()) {
        int row = StdRandom.uniform(n) + 1;
        int col = StdRandom.uniform(n) + 1;
        perc.open(row, col);
      }

      x[i] = (1.0 * perc.numberOfOpenSites()) / gridSize;
    }

    mean = StdStats.mean(x);
    stddev = StdStats.stddev(x);
    confidenceHi = mean + CONFIDENCE_95 * stddev / Math.sqrt(trials);
    confidenceLo = mean - CONFIDENCE_95 * stddev / Math.sqrt(trials);
  }
  
  // Sample mean of percolation threshold 
  public double mean()
  {
    return mean;        
  }

  // Sample standard deviation of percolation threshold
  public double stddev()
  {
    return stddev;
  }

  // Low  endpoint of 95% confidence interval
  public double confidenceLo()
  { 
    return confidenceLo;
  }

  // High endpoint of 95% confidence interval
  public double confidenceHi()
  {
    return confidenceHi;
  }

  // Test client (described below)
  public static void main(String[] args)
  {
  /*
    int n = Integer.parseInt(args[0]);
    int trials = Integer.parseInt(args[1]);

    Stopwatch sw = new Stopwatch();
    double t1 = sw.elapsedTime();
        
    PercolationStats ps = new PercolationStats(n, trials);

    double t2 = sw.elapsedTime();

    StdOut.println(t2 - t1);
  */
  }
}
