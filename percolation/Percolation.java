// import edu.princeton.cs.algs4.StdOut;
// import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

  private byte[] isopen;
  private int openSites;
  private final WeightedQuickUnionUF wqu;
  private final int topCore;
  private final int n;

  // Create n-by-n grid, with all sites blocked
  public Percolation(int n)
  {
    if (n <= 0)
      throw new java.lang.IllegalArgumentException("n must be higher than 0");
    this.n = n;
    int size = n * n;
    openSites = 0;
    wqu = new WeightedQuickUnionUF(size + 2);
        
    topCore = 0;
        
    isopen = new byte[size + 2];
    for (int row = 1; row <= n; row++) {
      for (int col = 1; col <= n; col++)
        isopen[convertIndexes(row, col)] = 0;
    }
    isopen[topCore] = 1;
  }

  private void checkIndexes(int row, int col)
  {
    if (row < 1 || row > n)
      throw new java.lang.IllegalArgumentException("row index out of bounds");
    if (col < 1 || col > n)
      throw new java.lang.IllegalArgumentException("col index out of bounds");
  }

  private int convertIndexes(int row, int col)
  {
    return (row - 1) * n + col;
  }

  // Open site (row, col) if it is not open already
  public void open(int row, int col)
  {
    checkIndexes(row, col);
    int index1 = convertIndexes(row, col);

    if (isopen[index1] > 0)
      return;

    int root1 = wqu.find(index1);
    isopen[index1] = 1;
    openSites++;

    int nbrsSize = 4;
    int[] nbrs = new int[nbrsSize];
    int nNbrs = 0;
    
    if (row == 1)
      nbrs[nNbrs++] = topCore;
    else
      nbrs[nNbrs++] = convertIndexes(row - 1, col);
    
    if (col != 1)
      nbrs[nNbrs++] = convertIndexes(row, col - 1);
      
    if (col != n)
      nbrs[nNbrs++] = convertIndexes(row, col + 1);
      
    if (row == n)
      isopen[root1] = 2;
    else
      nbrs[nNbrs++] = convertIndexes(row + 1, col);

    for (int i = 0; i < nNbrs; i++) {
      int index2 = nbrs[i];

      if (isopen[index2] > 0) {
        root1 = wqu.find(index1);
        int root2 = wqu.find(index2);
        if (isopen[root1] == 2)
          isopen[root2] = 2;
        if (isopen[root2] == 2)
          isopen[root1] = 2;
        wqu.union(index1, index2);
      }


    }
  }
  
  // Is site (row, col) open?
  public boolean isOpen(int row, int col)
  {
    checkIndexes(row, col);
    return isopen[convertIndexes(row, col)] > 0;
  }

  // is site (row, col) full?
  public boolean isFull(int row, int col)
  {
    checkIndexes(row, col);
    return wqu.connected(topCore, convertIndexes(row, col));
  }

  // Number of open sites
  public int numberOfOpenSites()
  {
    return openSites;
  }

  // Does the system percolate?
  public boolean percolates()
  {
    return isopen[wqu.find(topCore)] == 2;
  }


  // Test client (optional) 
  public static void main(String[] args)
  {
/*    
    In in = new In(args[0]);
    int n = in.readInt();

    Percolation perc = new Percolation(n);
    
    while (!in.isEmpty()) {
      int row = in.readInt();
      int col = in.readInt();
      perc.open(row, col);
    }
    
    StdOut.println("Percolates? " + perc.percolates());
*/
/*
    int n = 3;
    Percolation perc = new Percolation(n);
    perc.open(3,1);
    StdOut.println("(3,1) open? " + perc.isOpen(3,1));
    StdOut.println("(3,1) full? " + perc.isFull(3,1));

    perc.open(2,3);
    StdOut.println("(3,1) open? " + perc.isOpen(3,1));
    StdOut.println("(3,1) full? " + perc.isFull(3,1));

    perc.open(3,3);
    StdOut.println("(3,1) open? " + perc.isOpen(3,1));
    StdOut.println("(3,1) full? " + perc.isFull(3,1));

    perc.open(1,3);
    StdOut.println("(3,1) open? " + perc.isOpen(3,1));
    StdOut.println("(3,1) full? " + perc.isFull(3,1));

    perc.open(2,1);
    StdOut.println("(3,1) open? " + perc.isOpen(3,1));
    StdOut.println("(3,1) full? " + perc.isFull(3,1));

    perc.open(3,2);
    StdOut.println("(3,1) open? " + perc.isOpen(3,1));
    StdOut.println("(3,1) full? " + perc.isFull(3,1));
*/
  }
}
