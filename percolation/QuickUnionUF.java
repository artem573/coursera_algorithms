public class QuickUnionUF
{
    private int[] id;

    public QuickUnionUF(int N){
        id = new int[N];
        for(int i = 0; i < N; i++){
            id[i] = i;
        }
    }

    private int find(int i){
        while(i != id[i]){ i = id[i]; }
        return i;
    }

    private void validate(int p) {
        int n = id.length;
        if (p < 0 || p >= n) {
            throw new IllegalArgumentException("index " + p + " is not between 0 and " + (n-1));
        }
    }

    public boolean connected(int p, int q){
        return (find(p) == find(q));
    }

    public void union(int p, int q){
        int i = find(p);
        int j = find(q);

        id[i] = j;
    }
}

    
