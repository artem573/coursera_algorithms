// import edu.princeton.cs.algs4.In;
// import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.MinPQ;

import java.util.Deque;
import java.util.LinkedList;

public class Solver {
  private boolean unsolvable = false;
  private int minMoves = 0;
  private Deque<Board> solutionList = null;

  private class Node implements Comparable<Node> {
    int nMoves = 0;
    int priority = 0;

    private final Board board;
    private Node predecessor = null;

    Node(Board boardIn) {
      board = boardIn;
    }

    Node(Node pred, Board boardIn) {
      predecessor = pred;
      board = boardIn;
      nMoves = predecessor.nMoves + 1;
      priority = nMoves + board.manhattan();
    }

    public int compareTo(Node that) {
      if (that == null) { throw new NullPointerException("Argument is null"); }

      return Integer.compare(this.priority, that.priority);
    }
  };


  public Solver(Board initial) {
    if (initial == null) { throw new java.lang.IllegalArgumentException("Initial board is null"); }

    MinPQ<Node> nodes = new MinPQ<Node>(); 
    MinPQ<Node> nodesTwin = new MinPQ<Node>();

    Board initialTwin = initial.twin();

    Node initialNode = new Node(initial);
    Node initialTwinNode = new Node(initialTwin);
    
    
    nodes.insert(initialNode);
    nodesTwin.insert(initialTwinNode);

    while (true) {
      Node minNode = nodes.min();
      Board minBoard = minNode.board;
      if (minBoard.isGoal()) {
        minMoves = minNode.nMoves;
        break;
      }

      Node minTwinNode = nodesTwin.min();
      Board minTwinBoard = minTwinNode.board;
      if (minTwinBoard.isGoal()) {
        unsolvable = true;
        break;
      }

      nodes.delMin();      
      nodesTwin.delMin();


      for (Board nbrBoard : minBoard.neighbors()) {
        Node nbrNode = new Node(minNode, nbrBoard);
        if (minNode.predecessor == null) { nodes.insert(nbrNode); }
        else { if (!minNode.predecessor.board.equals(nbrNode.board)) { nodes.insert(nbrNode); } }
      }

      for (Board nbrTwinBoard : minTwinBoard.neighbors()) {
        Node nbrTwinNode = new Node(minTwinNode, nbrTwinBoard);
        if (minTwinNode.predecessor == null) { nodesTwin.insert(nbrTwinNode); }
        else { if (!minTwinNode.predecessor.board.equals(nbrTwinNode.board)) { nodesTwin.insert(nbrTwinNode); } }
      }
    }

    if (!unsolvable) {
      Node solutionNode = nodes.min();
      solutionList = new LinkedList<Board>();
      while (true) {
        solutionList.push(solutionNode.board);
        
        if (solutionNode.predecessor == null) { break; }
        else { solutionNode = solutionNode.predecessor; }
      }
    }
  }
    
  
  public boolean isSolvable() { return !unsolvable; }

  public int moves() {
    if (unsolvable) { return -1; }
    else { return minMoves; }
  }

  public Iterable<Board> solution() { return solutionList; }
  
  public static void main(String[] args) {
    /*
    // create initial board from file
    In in = new In(args[0]);
    int n = in.readInt();
    int[][] blocks = new int[n][n];
    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        blocks[i][j] = in.readInt();
    Board initial = new Board(blocks);

    StdOut.println(initial);

    // solve the puzzle
    Solver solver = new Solver(initial);
    
    // print solution to standard output
    if (!solver.isSolvable())
      StdOut.println("No solution possible");
    else {
      StdOut.println("Minimum number of moves = " + solver.moves() + "\n");
      for (Board board : solver.solution()) {
        StdOut.println(board);
      }
    }
    */
  }
}
  
