// import edu.princeton.cs.algs4.StdOut;
import java.util.Stack;

public class Board {
  private final int[][] array;
  private final int n;
  private int row0;
  private int col0;

  private enum Dir { LEFT, RIGHT, TOP, BOTTOM }

  public Board(int[][] blocks) {
    n = blocks.length;
    if (n < 2 || n >= 128) { throw new java.lang.IllegalArgumentException("n must be in [2; 128)"); }

    array = new int[n][n];
        
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        array[i][j] = blocks[i][j];
        if (array[i][j] == 0) {
          row0 = i;
          col0 = j;
        }
      }
    }
  }
    
  public int dimension() { return n; }

  public int hamming() {
    int notInPlace = -1;
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        if (array[i][j] != i*n + j + 1) { notInPlace++; }
      }
    }

    return notInPlace;
  }

  private int abs(int a)
  {
    if (a < 0) { return -a; }
    return a;
  }
    
  public int manhattan() {
    int dist = 0;
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        if (array[i][j] != 0) {
          int rightRow = (array[i][j] - 1) / n;
          int rightCol = (array[i][j] - 1) % n;
          dist += abs(i - rightRow) + abs(j - rightCol);
        }
      }
    }
        
    return dist;
  }
    
  public boolean isGoal() { return (manhattan() == 0); }

  private void swap(Board board, Dir dir, int rowFrom, int colFrom, boolean swapZero) {
    int rowTo = rowFrom;
    int colTo = colFrom;

    switch (dir) {
    case BOTTOM:
      rowTo++;
      if (swapZero) { board.row0++; }
      break;
    case TOP:
      rowTo--;
      if (swapZero) { board.row0--; }
      break;
    case RIGHT:
      colTo++;
      if (swapZero) { board.col0++; }
      break;
    case LEFT:
      colTo--;
      if (swapZero) { board.col0--; }
      break;
    }

    board.array[rowFrom][colFrom] = array[rowTo][colTo];
    board.array[rowTo][colTo] = array[rowFrom][colFrom];
  }
    
  public Board twin() {
    Board twinBoard = new Board(array);

    int rowFrom = (row0 == 0) ? 1 : 0;
    int colFrom = 0;
    boolean swapZero = false;
    swap(twinBoard, Dir.RIGHT, rowFrom, colFrom, swapZero);

    return twinBoard;
  }
    

  public boolean equals(Object y)
  {
    if (y == this) { return true; }
    if (y == null) { return false; }
    if (y.getClass() == this.getClass())
    {
      Board that = (Board) y;
      if (that.dimension() != this.dimension()) { return false; }

      for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
          if (that.array[i][j] != this.array[i][j]) { return false; }
        }
      }
      
      return true;
    }
    else
    {
      return false;
    }
  }

  public Iterable<Board> neighbors()
  {
    Stack<Board> nbrBoards = new Stack<Board>();
    Stack<Dir> dirs = new Stack<Dir>();
    
    if (row0 > 0) { dirs.push(Dir.TOP); }
    if (row0 < n - 1) { dirs.push(Dir.BOTTOM); }
    if (col0 > 0) { dirs.push(Dir.LEFT); }
    if (col0 < n - 1) { dirs.push(Dir.RIGHT); }
    
    for (Dir dir : dirs) {
      Board nbrBoard = new Board(array);
      boolean swapZero = true;
      swap(nbrBoard, dir, row0, col0, swapZero);
      nbrBoards.push(nbrBoard);
    }

    return nbrBoards;
  }

          
  public String toString() {
    
    StringBuilder sb = new StringBuilder();
    sb.append(Integer.toString(n) + "\n");
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        sb.append(" " + Integer.toString(array[i][j]));
      }
      sb.append("\n");
    }

    return sb.toString();
  }
    
  // public static void main(String[] args) {
  // }
}
