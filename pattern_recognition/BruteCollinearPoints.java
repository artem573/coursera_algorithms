import java.util.Arrays;
import java.util.ArrayList;

// import edu.princeton.cs.algs4.In;
// import edu.princeton.cs.algs4.StdOut;
// import edu.princeton.cs.algs4.StdDraw;


public class BruteCollinearPoints {
  private ArrayList<LineSegment> ls;

  public BruteCollinearPoints(Point[] pointsIn) {

    if (pointsIn == null) {
      throw new java.lang.IllegalArgumentException("array of points is null");
    }

    for (int i = 0; i < pointsIn.length; i++) {      
      if (pointsIn[i] == null) {
        throw new java.lang.IllegalArgumentException("Some point is null");
      }
    }

    Point[] points = new Point[pointsIn.length];
    for (int i = 0; i < pointsIn.length; i++) {
      points[i] = pointsIn[i];
    }
    
    Arrays.sort(points);

    for (int i = 0; i < points.length - 1; i++) {      
      if (points[i].compareTo(points[i+1]) == 0) {
        throw new java.lang.IllegalArgumentException("There are duplicate points");
      }
    }

    ls = new ArrayList<LineSegment>();

    for (int p = 0; p < points.length - 3; p++) {      
      for (int q = p + 1; q < points.length - 2; q++) {
        double slopePQ = points[p].slopeTo(points[q]);
        for (int r = q + 1; r < points.length - 1; r++) {
          double slopePR = points[p].slopeTo(points[r]);
          for (int s = r + 1; s < points.length; s++) {
            double slopePS = points[p].slopeTo(points[s]);
            if ((slopePQ == slopePR) && (slopePR == slopePS)) {
              ls.add(new LineSegment(points[p], points[s]));
            } 
          }
        }
      }
    }
  }
  
  public int numberOfSegments() { return ls.size(); }

  public LineSegment[] segments() {
    LineSegment[] ret = new LineSegment[ls.size()];
    int i = 0;
    for (LineSegment iLS : ls) {
      ret[i++] = iLS;
    }

    return ret;
  }

  
  public static void main(String[] args) {
/*
    // read the n points from a file
    In in = new In(args[0]);
    int n = in.readInt();
    Point[] points = new Point[n];
    for (int i = 0; i < n; i++) {
      int x = in.readInt();
      int y = in.readInt();
      points[i] = new Point(x, y);
    }
    
    // draw the points
    StdDraw.enableDoubleBuffering();
    StdDraw.setXscale(0, 32768);
    StdDraw.setYscale(0, 32768);
    for (Point p : points) {
      p.draw();
    }
    StdDraw.show();
    
    // print and draw the line segments
    BruteCollinearPoints collinear = new BruteCollinearPoints(points);
    for (LineSegment segment : collinear.segments()) {
      StdOut.println(segment);
      segment.draw();
    }
    StdDraw.show();
*/
  }
}
