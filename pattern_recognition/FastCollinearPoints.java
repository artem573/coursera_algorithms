import java.util.Arrays;
import java.util.ArrayList;

// import edu.princeton.cs.algs4.In;
// import edu.princeton.cs.algs4.StdOut;
// import edu.princeton.cs.algs4.StdDraw;


public class FastCollinearPoints {
  private ArrayList<LineSegment> lineSegments;

  public FastCollinearPoints(Point[] inputPoints) {
    if (inputPoints == null) { throw new java.lang.IllegalArgumentException("The array of points is null"); }
    for (int i = 0; i < inputPoints.length; i++) {      
      if (inputPoints[i] == null) {
        throw new java.lang.IllegalArgumentException("A point is null");
      }
    }

    Point[] basePoints = new Point[inputPoints.length];
    for (int i = 0; i < inputPoints.length; i++) {
      basePoints[i] = inputPoints[i];
    }

    Arrays.sort(basePoints);

    for (int i = 0; i < basePoints.length - 1; i++) {      
      if (basePoints[i].compareTo(basePoints[i+1]) == 0) {
        throw new java.lang.IllegalArgumentException("There are duplicate points");
      }
    }

    lineSegments = new ArrayList<LineSegment>();

    for (Point basePoint : basePoints) {
      Point[] points = new Point[basePoints.length];
      for (int i = 0; i < basePoints.length; i++) {
        points[i] = basePoints[i];
      }

      Arrays.sort(points, basePoint.slopeOrder());

      double[] slopes = new double[points.length];
      for (int j = 0; j < points.length; j++) {
        slopes[j] = basePoint.slopeTo(points[j]);
      }

      int j = 0;
      while (j < points.length - 2) {
        int k = j + 1;
        while ((k < points.length) && (slopes[j] == slopes[k])) { k++; }
        if ((k - j >= 3) && (basePoint.compareTo(points[j]) < 0))
          lineSegments.add(new LineSegment(basePoint, points[k - 1]));
        j = k;
      }
    }
  }
    
  public int numberOfSegments() { return lineSegments.size(); }

  public LineSegment[] segments() {
    LineSegment[] ret = new LineSegment[lineSegments.size()];
    int i = 0;
    for (LineSegment lineSegment : lineSegments) {
      ret[i++] = lineSegment;
    }
    
    return ret;
  }
  

  public static void main(String[] args) {
/*
    //Point p = new Point(0, 8);
    //Point q = new Point(0, 8);
    //Point r = new Point(0, 8);
    //StdOut.println(p.slopeTo(q));
    //StdOut.println(p.slopeTo(r));

    // read the n points from a file
    In in = new In(args[0]);
    int n = in.readInt();
    Point[] points = new Point[n];
    for (int i = 0; i < n; i++) {
      int x = in.readInt();
      int y = in.readInt();
      points[i] = new Point(x, y);
    }
    
    // draw the points
    StdDraw.enableDoubleBuffering();
    StdDraw.setXscale(0, 32768);
    StdDraw.setYscale(0, 32768);
    for (Point p : points) {
      p.draw();
    }
    StdDraw.show();

    // print and draw the line segments
    FastCollinearPoints collinear = new FastCollinearPoints(points);
    for (LineSegment segment : collinear.segments()) {
      StdOut.println(segment);
      segment.draw();
    }
    StdDraw.show();
*/  
  }
}
