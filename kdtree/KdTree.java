import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdOut;
import java.util.Stack;

public class KdTree
{
  private static final double XMIN = 0.0;
  private static final double XMAX = 1.0;
  private static final double YMIN = 0.0;
  private static final double YMAX = 1.0;
  private static final boolean XCOORD = true;
  private static final boolean YCOORD = false;
  private Node root;
  private int size = 0;

  private static class Node
  {
    private final Point2D point;
    private final RectHV rect;
    private Node lb;
    private Node rt;

    public Node(Point2D p, RectHV rectangle)
    {
      point = p;
      rect = rectangle;
    }
  };

  public KdTree() { }
  
  public boolean isEmpty() { return size == 0; }
  
  public int size() { return size; }

  private void insertInNode(Node node, boolean coord, Point2D p)
  {
    if (isEmpty()) {
      size++;
      root = new Node(p, new RectHV(XMIN, YMIN, XMAX, YMAX));
      return;
    }
    if (node.point.equals(p)) { return; }

    if (coord == XCOORD) {
        if (p.x() < node.point.x()) {
          if (node.lb != null) { insertInNode(node.lb, !coord, p); }
          else {
            node.lb = new Node(p, new RectHV(node.rect.xmin(), node.rect.ymin(), node.point.x(), node.rect.ymax()));
            size++;
            return;
          }
        } else {
          if (node.rt != null) { insertInNode(node.rt, !coord, p); }
          else {
            node.rt = new Node(p, new RectHV(node.point.x(), node.rect.ymin(), node.rect.xmax(), node.rect.ymax()));
            size++;
            return;
          }
        }
      } else {
        if (p.y() < node.point.y()) {
          if (node.lb != null) { insertInNode(node.lb, !coord, p); }
          else {
            node.lb = new Node(p, new RectHV(node.rect.xmin(), node.rect.ymin(), node.rect.xmax(), node.point.y()));
            size++;
            return;
          }
        } else {
          if (node.rt != null) { insertInNode(node.rt, !coord, p); }
          else {
            node.rt = new Node(p, new RectHV(node.rect.xmin(), node.point.y(), node.rect.xmax(), node.rect.ymax()));
            size++;
            return;
          }
        }
      }
    return;
  }
  
  public void insert(Point2D p)
  {
    if (p == null) { throw new java.lang.IllegalArgumentException("The argument of KdTree.insert is null"); }
    insertInNode(root, XCOORD, p);
  }

  private boolean containsInNode(Node node, boolean coord, Point2D p)
  {
    if (node == null) { return false; }
    if (node.point.equals(p)) { return true; }
    if (((coord == XCOORD) && (p.x() < node.point.x())) ||
        ((coord == YCOORD) && (p.y() < node.point.y()))) {
      return containsInNode(node.lb, !coord, p);
    } else { return containsInNode(node.rt, !coord, p); }
  }
  
  public boolean contains(Point2D p)
  {
    if (p == null) { throw new java.lang.IllegalArgumentException("The argument of KdTree.contains is null"); }
    return containsInNode(root, XCOORD, p);
  }   
  
  private void drawNode(Node node, boolean coord)
  {
    if (node == null) { return; }
    StdDraw.setPenRadius(0.01);
    StdDraw.setPenColor(StdDraw.BLACK);
    node.point.draw();

    StdDraw.setPenRadius();
    if (coord == XCOORD) {
      StdDraw.setPenColor(StdDraw.RED);
      StdDraw.line(node.point.x(), node.rect.ymin(), node.point.x(), node.rect.ymax());
    } else {
      StdDraw.setPenColor(StdDraw.BLUE);
      StdDraw.line(node.rect.xmin(), node.point.y(), node.rect.xmax(), node.point.y());
    }
    
    drawNode(node.lb, !coord);
    drawNode(node.rt, !coord);
  }
  
  public void draw() { drawNode(root, XCOORD); }

  private void rangeStack(Node node, RectHV rect, Stack<Point2D> points)
  {
    if (node == null) { return; }
    if (!rect.intersects(node.rect)) { return; }
    if (rect.contains(node.point)) { points.push(node.point); }
    rangeStack(node.lb, rect, points);
    rangeStack(node.rt, rect, points);
  }
  
  public Iterable<Point2D> range(RectHV rect)
  {
    if (rect == null) { throw new java.lang.IllegalArgumentException("The argument of KdTree.range is null"); }
    Stack<Point2D> points = new Stack<Point2D>();
    rangeStack(root, rect, points);
    return points;
  }
    
  private Point2D nearestPoint(Node node, Point2D np, Point2D p)
  {
    if (node == null) { return np; }
    
    double dist = node.point.distanceSquaredTo(p);
    double ndist = np.distanceSquaredTo(p);
    if (dist < ndist)
    {
      np = node.point;
      ndist = dist;
    }

    Node node1 = node.lb;
    Node node2 = node.rt;
    double dist1 = 0;
    double dist2 = 0;
    if (node1 != null) { dist1 = node1.rect.distanceSquaredTo(p); }
    if (node2 != null) { dist2 = node2.rect.distanceSquaredTo(p); }
    if ((node1 != null) && (dist1 > ndist)) { node1 = null; }
    if ((node2 != null) && (dist2 > ndist)) { node2 = null; }
    if ((node1 != null) && (node2 != null)) {
      if (dist1 > dist2) {
        node1 = node.rt;
        node2 = node.lb;
        double temp = dist1;
        dist1 = dist2;
        dist2 = temp;
      }
    }

    np = nearestPoint(node1, np, p);
    ndist = np.distanceSquaredTo(p);
    if ((node2 != null) && (dist2 > ndist)) { node2 = null; }
    np = nearestPoint(node2, np, p);
    return np;
  }
  
  public Point2D nearest(Point2D p)
  {
    if (p == null) { throw new java.lang.IllegalArgumentException("The argument of KdTree.nearest is null"); }
    if (isEmpty()) { return null; }

    return nearestPoint(root, root.point, p);
  }
}
