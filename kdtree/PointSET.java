import java.util.TreeSet;
import java.util.Stack;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;

public class PointSET
{
  private final TreeSet<Point2D> treeSet;
  
  public PointSET() { treeSet = new TreeSet<Point2D>(); }
  
  public boolean isEmpty() { return treeSet.isEmpty(); }
  
  public int size() { return treeSet.size(); }
  
  public void insert(Point2D p)
  {
    if (p == null) { throw new java.lang.IllegalArgumentException("The argument of PointSET.insert is null"); }
    treeSet.add(p);
  }
  
  public boolean contains(Point2D p)
  {
    if (p == null) { throw new java.lang.IllegalArgumentException("The argument of PointSET.contains is null"); }
    return treeSet.contains(p);
  }   
  
  public void draw()
  {
    for (Point2D pnt : treeSet) {
      pnt.draw();
    }
  }
  
  public Iterable<Point2D> range(RectHV rect)
  {
    if (rect == null) { throw new java.lang.IllegalArgumentException("The argument of PointSET.range is null"); }
    Stack<Point2D> points = new Stack<Point2D>();
    for (Point2D pnt : treeSet) {
      if (rect.contains(pnt)) { points.push(pnt); }
    }
    return points;
  }
    
  public Point2D nearest(Point2D p)
  {
    if (p == null) { throw new java.lang.IllegalArgumentException("The argument of PointSET.nearest is null"); }
    if (isEmpty()) { return null; }
    Point2D nearestPoint = treeSet.first();
    double minDist = p.distanceSquaredTo(nearestPoint);
    for (Point2D pnt : treeSet) {
      double dist = p.distanceSquaredTo(pnt);
      if (minDist > dist) {
        minDist = dist;
        nearestPoint = pnt;
      }
    }
    return nearestPoint;
   }
}
