import java.util.ArrayList;
import edu.princeton.cs.algs4.In;
// import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.DirectedCycle;
import java.util.HashSet;
import java.util.HashMap;

public class WordNet
{
  private static final boolean RETURN_DISTANCE = true;
  private final SAP sap;
  private final HashSet<String> nounsList;
  private final HashMap<String, ArrayList<Integer>> nounsPos;
  private final String[] data;
   
  // constructor takes the name of the two input files
  public WordNet(String synsets, String hypernyms)
  {
    if ((synsets == null) || (hypernyms == null)) { throw new java.lang.IllegalArgumentException("WordNet constructor argument is null"); }
    In inSynsets = new In(synsets);
    In inHypernyms = new In(hypernyms);

    data = inSynsets.readAllLines();
    nounsList = new HashSet<String>();
    nounsPos = new HashMap<String, ArrayList<Integer>>();
    for (int i = 0; i < data.length; i++)
    {
      data[i] = data[i].split(",")[1];
      for (String item: data[i].split(" "))
      {
        nounsList.add(item);
        if (nounsPos.get(item) == null) { nounsPos.put(item, new ArrayList<Integer>()); }
        ArrayList<Integer> value = nounsPos.get(item);
        value.add(i);
      }
    }

    Digraph G = new Digraph(data.length);
    
    while (!inHypernyms.isEmpty())
    {
      String line = inHypernyms.readLine();
      String[] str = line.split(",");
      int core = Integer.parseInt(str[0]);
      for (int j = 1; j < str.length; j++)
      {
        int node = Integer.parseInt(str[j]);
        G.addEdge(core, node);
      }
    }

    byte rootCount = 0;
    for (int i = 0; i < G.V(); i++)
    {
      if (G.outdegree(i) == 0)
      {
        rootCount++;
        if (rootCount > 1) { throw new java.lang.IllegalArgumentException("Graph has more than one root"); }
      }
    }

    DirectedCycle dc = new DirectedCycle(G);
    if (dc.hasCycle()) { throw new java.lang.IllegalArgumentException("WordNet graph has a cycle"); }
    sap = new SAP(G);
  }
  
  // returns all WordNet nouns
  public Iterable<String> nouns() { return nounsList; }
    
  // is the word a WordNet noun?
  public boolean isNoun(String word)
  {
    if (word == null) { throw new java.lang.IllegalArgumentException("WordNet isNoun argument is null"); }
    if (nounsList.contains(word)) { return true; }
    return false;
  }

  private void validateNoun(String noun)
  {
    if (noun == null) { throw new java.lang.IllegalArgumentException("WordNet argument is null"); }
    if (!isNoun(noun)) { throw new java.lang.IllegalArgumentException("WordNet argument isn't in list"); }
  }

  // distance between nounA and nounB (defined below)
  public int distance(String nounA, String nounB)
  {
    validateNoun(nounA);
    validateNoun(nounB);

    ArrayList<Integer> posA = nounsPos.get(nounA); 
    ArrayList<Integer> posB = nounsPos.get(nounB);
      
    return sap.length(posA, posB);
  }

  // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
  // in a shortest ancestral path (defined below)
  public String sap(String nounA, String nounB)
  {
    validateNoun(nounA);
    validateNoun(nounB);

    ArrayList<Integer> posA = nounsPos.get(nounA); 
    ArrayList<Integer> posB = nounsPos.get(nounB);
      
    return data[sap.ancestor(posA, posB)];
  }

  // do unit testing of this class
  public static void main(String[] args)
  {
/*
    WordNet wn = new WordNet(args[0], args[1]);
    String nounA = args[2];
    String nounB = args[3];
    StdOut.println(nounA + " isNoun? " + wn.isNoun(nounA));
    StdOut.println(nounB + " isNoun? " + wn.isNoun(nounB));
    StdOut.println(wn.distance(nounA, nounB));
    StdOut.println(wn.sap(nounA, nounB));
*/
  }
}
