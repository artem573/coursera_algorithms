// import edu.princeton.cs.algs4.In;
// import edu.princeton.cs.algs4.StdOut;

public class Outcast
{
  private final WordNet wn;
  
  public Outcast(WordNet wordnet)
  {
    if (wordnet == null) { throw new java.lang.IllegalArgumentException("Outcast constructor has null argument"); }
    wn = wordnet;
  }

  public String outcast(String[] nouns)
  {
    if (nouns == null) { throw new java.lang.IllegalArgumentException("Outcast.outcast has null argument"); }
    for (String noun: nouns)
    {
      if (!wn.isNoun(noun)) { throw new java.lang.IllegalArgumentException("Element of Outcast.outcast argument isn't presented in WordNet"); }
    }

    int[] dists = new int[nouns.length];
    for (int i = 0; i < nouns.length; i++)
    {
      dists[i] = 0;
    }

    for (int i = 0; i < nouns.length; i++)
    {
      for (int j = i + 1; j < nouns.length; j++)
      {
        int tempDist = wn.distance(nouns[i], nouns[j]);
        dists[i] += tempDist;
        dists[j] += tempDist;
      }
    }

    int maxDist = -1;
    String word = "";
    for (int i = 0; i < nouns.length; i++)
    {
      if (maxDist < dists[i])
      {
        maxDist = dists[i];
        word = nouns[i];
      }
    }
    return word;
  }
  
  public static void main(String[] args)
  {
/*
    WordNet wordnet = new WordNet(args[0], args[1]);
    Outcast outcast = new Outcast(wordnet);
    for (int t = 2; t < args.length; t++)
    {
      In in = new In(args[t]);
      String[] nouns = in.readAllStrings();
      StdOut.println(args[t] + ": " + outcast.outcast(nouns));
    }
*/
  }
}
