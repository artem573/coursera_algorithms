import java.util.Objects;
import edu.princeton.cs.algs4.Digraph;
// import edu.princeton.cs.algs4.StdOut;
// import edu.princeton.cs.algs4.StdIn;
// import edu.princeton.cs.algs4.In;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Queue;
// import java.util.Collections;

public class SAP
{
  private static final boolean RETURN_LENGTH = true;
  private static final boolean RETURN_ANCESTOR = false;
  private final Digraph g;
  // private ArrayList<Integer> cachedV;
  // private ArrayList<Integer> cachedW;
  private int cachedLength;
  private int cachedAncestor;
  
  // constructor takes a digraph (not necessarily a DAG)
  public SAP(Digraph G)
  {
    if (G == null) { throw new java.lang.IllegalArgumentException("SAP constructor has null argument"); }
    g = new Digraph(G);
  }

  /*
  private boolean isListsEqual(ArrayList<Integer> v, ArrayList<Integer> w)
  {
    if ((v == null) || (w == null) || (v.size() != w.size())) { return false; }

    // to avoid messing the order of the lists we will use a copy
    v = new ArrayList<Integer>(v);
    w = new ArrayList<Integer>(w);

    Collections.sort(v);
    Collections.sort(w);      
    return v.equals(w);
  }
  */
  
  private int ancestorLength(Iterable<Integer> v, Iterable<Integer> w, boolean ret)
  {
    for (Integer itemV: v)
    {
      if (itemV == null) { throw new java.lang.IllegalArgumentException("SAP argument is null"); }
      if ((itemV < 0) || (itemV >= g.V())) { throw new java.lang.IllegalArgumentException("SAP argument isn't in [0..V] range"); }
    }
    for (Integer itemW: w)
    { 
      if (itemW == null) { throw new java.lang.IllegalArgumentException("SAP argument is null"); }
      if ((itemW < 0) || (itemW >= g.V())) { throw new java.lang.IllegalArgumentException("SAP argument isn't in [0..V] range"); }
    }

    /*
    if (isListsEqual(v, cachedV) && isListsEqual(w, cachedW))
    {
      if (ret == RETURN_LENGTH) { return cachedLength; }
      else { return cachedAncestor; }
    }
    
    cachedV = new ArrayList<Integer>(v);
    cachedW = new ArrayList<Integer>(w);
    */
    
    for (int itemV: v)
    {
      for (int itemW: w)
      {
        if (Objects.equals(itemV, itemW))
        {
          cachedLength = 0;
          cachedAncestor = itemV;
          if (ret == RETURN_LENGTH) { return cachedLength; }
          else { return cachedAncestor; }
        }
      }
    }

    int dist = g.E() + 1;
    int vertex = -1;

    int[] distV = new int[g.V()];
    int[] distW = new int[g.V()];
    for (int i = 0; i < g.V(); i++)
    {
      distV[i] = -1;
      distW[i] = -1;
    }
    
    Queue<Integer> queue = new LinkedList<>();
    Queue<Boolean> flags = new LinkedList<>();
    final Boolean V_FLAG = Boolean.TRUE;
    final Boolean W_FLAG = Boolean.FALSE;

    for (int item: v)
    {
      queue.add(item);
      flags.add(V_FLAG);
      distV[item] = 0;
    }

    for (int item: w)
    {
      queue.add(item);
      flags.add(W_FLAG);
      distW[item] = 0;
    }

    while (queue.size() > 0)
    {
      int pos = queue.remove();
      boolean flag = flags.remove();
      // StdOut.println("pos: " + pos + ", flag: " + flag + ", l: " + dist + ", v: " + vertex);

      for (int item: g.adj(pos))
      {
        if (flag == V_FLAG)
        {
          if ((distV[item] == -1) && (distV[pos] < dist))
          {
            distV[item] = distV[pos] + 1;
            if (distW[item] >= 0)
            {
              int tempDist = distV[item] + distW[item];
              if (tempDist < dist)
              {
                dist = tempDist;
                vertex = item;
              }
            }
            queue.add(item);
            flags.add(V_FLAG);
          }
        }
        else
        {
          if ((distW[item] == -1) && (distW[pos] < dist))
          {
            distW[item] = distW[pos] + 1;
            if (distV[item] >= 0)
            {
              int tempDist = distV[item] + distW[item];
              if (tempDist < dist)
              {
                dist = tempDist;
                vertex = item;
              }
            }
            queue.add(item);
            flags.add(W_FLAG);
          }
        }
      }
    }

    if (vertex == -1) { dist = -1; }

    cachedLength = dist;
    cachedAncestor = vertex;
    if (ret == RETURN_LENGTH) { return cachedLength; }
    else { return cachedAncestor; }
  }
  
  // length of shortest ancestral path between v and w; -1 if no such path
  public int length(int v, int w)
  {
    ArrayList<Integer> vList = new ArrayList<>();
    ArrayList<Integer> wList = new ArrayList<>();
    vList.add(v);
    wList.add(w);
    return length(vList, wList);
  }  

  // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
  public int length(Iterable<Integer> v, Iterable<Integer> w)
  {
    if ((v == null) || (w == null)) { throw new java.lang.IllegalArgumentException("SAP.length has null argument"); }
    return ancestorLength(v, w, RETURN_LENGTH);
  }

  // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
  public int ancestor(int v, int w)
  {
    ArrayList<Integer> vList = new ArrayList<>();
    ArrayList<Integer> wList = new ArrayList<>();
    vList.add(v);
    wList.add(w);
    return ancestor(vList, wList);
  }
    
  // a common ancestor that participates in shortest ancestral path; -1 if no such path
  public int ancestor(Iterable<Integer> v, Iterable<Integer> w)
  {
    if ((v == null) || (w == null)) { throw new java.lang.IllegalArgumentException("SAP.ancestor has null argument"); }
    return ancestorLength(v, w, RETURN_ANCESTOR);
  }

  // do unit testing of this class
  public static void main(String[] args)
  {
/*
    In in = new In(args[0]);
    Digraph G = new Digraph(in);
    SAP sap = new SAP(G);
    
    while (!StdIn.isEmpty()) {
      int v = StdIn.readInt();
      int w = StdIn.readInt();
      int length   = sap.length(v, w);
      int ancestor = sap.ancestor(v, w);
      StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
    }
*/
  }
}
