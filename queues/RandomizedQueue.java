// import edu.princeton.cs.algs4.StdIn;
// import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import java.util.Iterator;

public class RandomizedQueue<Item> implements Iterable<Item> {
  private Item[] q;
  private int n = 0;

  public RandomizedQueue() { q = (Item[]) new Object[1]; }

  public boolean isEmpty() { return n == 0; }

  public int size() { return n; }

  public void enqueue(Item a) {
    if (a == null) { throw new java.lang.IllegalArgumentException("argument is null"); }
    if (n == q.length) { resize(2 * q.length); }
    q[n++] = a;
  }

  private void resize(int length) {
    Item[] copy = (Item[]) new Object[length];
    for (int i = 0; i < n; i++) {
      copy[i] = q[i];
    }

    q = copy;
  }

  public Item dequeue() {
    if (isEmpty()) { throw new java.util.NoSuchElementException("the deque is empty"); }
    int ind = StdRandom.uniform(n);
    Item item = q[ind];
    q[ind] = q[--n];
    q[n] = null;
    if (n > 0 && n == q.length/4) { resize(q.length/2); }
    return item;
  }

  public Item sample() {
    if (isEmpty()) { throw new java.util.NoSuchElementException("the deque is empty"); }
    int ind = StdRandom.uniform(n);
    return q[ind];
  }

  public Iterator<Item> iterator() { return new ArrayIterator(); }

  private class ArrayIterator implements Iterator<Item> {
    private final int s = size();
    private final int[] index;
    private int current = 0;

    public ArrayIterator() {
      index = new int[s];
      for (int i = 0; i < s; i++) {
        index[i] = i;
      }
      
      StdRandom.shuffle(index);
    }

    public boolean hasNext() { return current < s; }
    public void remove() { throw new java.lang.UnsupportedOperationException(" remove operation does not implemented"); }
    public Item next() {
      if (current == s) { throw new java.util.NoSuchElementException("there are no more elements to return"); }
      return q[index[current++]];
    }
  }

  public static void main(String[] args) {
    /*
    RandomizedQueue<Integer> queue = new RandomizedQueue<Integer>();
    queue.enqueue(0);
    queue.enqueue(1);
    queue.enqueue(5);
    StdOut.println(queue.size());

    for ( int q : queue) {
      StdOut.println(q);
    }

    for ( int q : queue) {
      StdOut.println(q);
    }

    int size = queue.size();
    for ( int i = 0; i < size; i++) {
      StdOut.println(queue.dequeue());
    } 
    */      
  }
}    

