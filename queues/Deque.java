// import edu.princeton.cs.algs4.StdOut;
import java.util.Iterator;


public class Deque<Item> implements Iterable<Item> {
  private Node first = null;
  private Node last = null;
  private int s = 0;
  
  private class Node {
    private Item item;
    private Node next;
    private Node prev;
  }

  public Deque() { }
  
  public boolean isEmpty() { return s == 0; }
  
  public int size() { return s; }

  public void addFirst(Item item) {
    if (item == null) { throw new java.lang.IllegalArgumentException("argument is null"); }
    Node oldFirst = first;
    first = new Node();
    first.item = item;
    first.next = oldFirst;
    first.prev = null;
    s++;
    if (s == 1) { last = first; }
    if (s > 1) { first.next.prev = first; }
  }

  public void addLast(Item item) {
    if (item == null) { throw new java.lang.IllegalArgumentException("argument is null"); }
    Node oldLast = last;
    last = new Node();
    last.item = item;
    last.next = null;
    last.prev = oldLast;
    s++;
    if (s == 1) { first = last; }
    if (s > 1) { last.prev.next = last; }
  }
  
  public Item removeFirst() {
    if (isEmpty()) { throw new java.util.NoSuchElementException("the deque is empty"); }
    Item item = first.item;
    first = first.next;
    s--;
    if (isEmpty()) { last = null; }
    else { first.prev = null; }
    return item;
  }

  public Item removeLast() {
    if (isEmpty()) { throw new java.util.NoSuchElementException("the deque is empty"); }
    Item item = last.item;
    last = last.prev;
    s--;
    if (isEmpty()) { first = null; }
    else { last.next = null; }
    return item;
  }

  public Iterator<Item> iterator() { return new ListIterator(); }

  private class ListIterator implements Iterator<Item> {
    private Node current = first;

    public boolean hasNext() { return current != null; }
    public void remove() { throw new java.lang.UnsupportedOperationException(" remove operation does not implemented"); }
    public Item next() {
      if (current == null) { throw new java.util.NoSuchElementException("there are no more elements to return"); }
      Item item = current.item;
      current = current.next;
      return item;
    }
  }

  
  public static void main(String[] args) {  // unit testing (optional)
    /*
    Deque<Integer> deque = new Deque<Integer>();
    deque.addFirst(1);
    StdOut.println(deque.size());
    deque.removeFirst();
    */
  }
}


